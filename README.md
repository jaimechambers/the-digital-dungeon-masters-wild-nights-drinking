# The Digital Dungeon Master's Wild Nights Drinking for Foundry VTT

This is a rough draft...a work in progress, if you will, for a Foundry VTT module that provides a collection of tavern themed materials for use in Foundry VTT.

[[_TOC_]]

## Installation

To install, follow these instructions:

1. Open your instance of **Foundry VTT** and login
2. From within the **Foundry Virtual Tabletop - Configuration and Setup**, click on the **Add-on Modules** tab
3. Click on the **Install Module** button in the bottom left-hand corner
4. Paste the URL provided below into the **Manifest URL**: box
5. Click the **Install** button

**Manifest URL:** https://gitlab.com/jaimechambers/the-digital-dungeon-masters-wild-nights-drinking/-/raw/main/module.json

## Why Wild Nights Drinking?

Are you a Dungeon Master in need of a tavern to bring your stalwart party of adventurers together? Do you need a special place to provide some downtime for your campaign? How about a seedy dive to get a good bar brawl going? Maybe your players just want to kick back a few for a wild night of gaming fun! Look no further! The Digital Dungeon Master's Wild Nights Drinking module for Foundry VTT is here for you! You'll find everything you need to setup the tavern, inn, dive, bar, alehouse, lounge, pub, saloon, taphouse, watering hole, or speak easy of your dreams. 

## What's It Do?

This is a collection of drinking themed [**Actors**](https://foundryvtt.com/article/actors/), [**Items**](https://foundryvtt.com/article/items/), [**Journal Entries**](https://foundryvtt.com/article/journal/), [**Macros**](https://foundryvtt.com/article/macros/), [**Playlists**](https://foundryvtt.com/article/playlists/), [**Rollable Tables**](https://foundryvtt.com/article/roll-tables/), and [**Scenes**](https://foundryvtt.com/article/scenes/) for use in [**Foundry VTT**](https://foundryvtt.com/). Once installed, the module will create a series of "**TDDM-WND**" compendiums. You can import all of the various elements and use them within your campaign. 

## Usage

Once you have installed the module, you will find a number of new "**TDDM-WND**" compendiums added to your **Compendium Packs** tab within Foundry VTT. You can view the contents of the compendiums by clicking on them. You can also import the contents to your Foundry VTT campaign. 

**Please note**, the compendiums represent many different types of Foundry VTT materials. You can import [**Actors**](https://foundryvtt.com/article/actors/), [**Items**](https://foundryvtt.com/article/items/), [**Journal Entries**](https://foundryvtt.com/article/journal/), [**Macros**](https://foundryvtt.com/article/macros/), [**Playlists**](https://foundryvtt.com/article/playlists/), [**Rollable Tables**](https://foundryvtt.com/article/roll-tables/), and [**Scenes**](https://foundryvtt.com/article/scenes/). These items, when imported, can be found under their respective tabs within your campaign. 

## Included Compendiums

- **Wild Nights Drinking - Drinks:** Over 50 custom crafted items to serve up at your favorite watering hole. Each includes descriptions and effects to help you roleplay with your players. You get the Items and the Journal Entries and a Rollable Table to randomly pull an item. The table points to the compendiums, so you don't need to import items unless you want to. You even get a Macro to roll!
   - Note! This works really well with [Loot Sheet NPC 5E](https://foundryvtt.com/packages/lootsheetnpc5e). You can easily use the included Rollable Table to populate a Loot Sheet NPC with drinks for sale. Every item has a weight and a price associated to it. Let your players buy their own booze!
- **Wild Nights Drinking - Effects:** Over 100 easily randomized effects. It's like a Wild Magic Surge table...but for drinking! Want to see how your characters are impacted by swilling back too much hooch. There's a table for that.

## Bug Reporting

This module supports and utilizes the [**Bug Reporter**](https://foundryvtt.com/packages/bug-reporter) add-on module for Foundry VTT. It is not a dependancy of this module, but it is recommended. You can submit any issues via the **Post Bug** button available from the **Game Settings** tab within Foundry VTT.

**Direct Submission:** You can also submit directly via GitLab. Please submit your issues [**HERE**](https://gitlab.com/jaimechambers/the-digital-dungeon-masters-wild-nights-drinking/-/issues). 
